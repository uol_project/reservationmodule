package com.uolproject.reservationmodule.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Reservation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
		
	@Column(name = "description")
	private String description;
	
    @Column(name = "last_modified")
    private LocalDateTime lastModified;
    
    @JsonIgnore //don't include in the JSON returned via REST controller.
    @Column(name = "creation_date", updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "reservation_start_date")
    private LocalDateTime reservationStartAt;
    
    @Column(name = "duration")
    private Integer durationInMins;
    
    //@Embedded
    //private CustomerDetail customerDetail;
    
    @OneToOne(cascade = CascadeType.ALL)
    private CustomerDetail customerDetail;
		
    //incentive
    //link to incentive table - hold incentive ID here as key
    
    //customer
    //create a customer record - hold ID here as key
    
    //agent (e.g. host, table or default)
	
    //status (open / reserved)
    
    
    public CustomerDetail getCustomerDetail() {
		return customerDetail;
	}

	public void setCustomerDetail(CustomerDetail customerDetail) {
		this.customerDetail = customerDetail;
	}

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getReservationStartAt() {
		return reservationStartAt;
	}

	public void setReservationStartAt(LocalDateTime reservationStartAt) {
		this.reservationStartAt = reservationStartAt;
	}

	public Integer getDurationInMins() {
		return durationInMins;
	}

	public void setDurationInMins(Integer durationInMins) {
		this.durationInMins = durationInMins;
	}

}
