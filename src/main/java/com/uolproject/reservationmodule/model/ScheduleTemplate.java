package com.uolproject.reservationmodule.model;

import java.time.LocalDateTime;

	
public class ScheduleTemplate {
	
	private LocalDateTime scheduleDate;
	private LocalDateTime startTime;
	private Integer durationInMins;
	private Integer appointmentCount;
	private LocalDateTime lastModified;
	private LocalDateTime createdAt;
	

	public LocalDateTime getScheduleDate() {
		return scheduleDate;
	}
	public void setScheduleDate(LocalDateTime scheduleDate) {
		this.scheduleDate = scheduleDate;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public Integer getDurationInMins() {
		return durationInMins;
	}
	public void setDurationInMins(Integer durationInMins) {
		this.durationInMins = durationInMins;
	}
	public Integer getAppointmentCount() {
		return appointmentCount;
	}
	public void setAppointmentCount(Integer appointmentCount) {
		this.appointmentCount = appointmentCount;
	}
	public LocalDateTime getLastModified() {
		return lastModified;
	}
	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	@Override
	public String toString() {
		return "ScheduleTemplate [scheduleDate=" + scheduleDate + ", startTime=" + startTime
				+ ", durationInMins=" + durationInMins + ", appointmentCount=" + appointmentCount + ", lastModified="
				+ lastModified + ", createdAt=" + createdAt + "]";
	}
	
	

}
