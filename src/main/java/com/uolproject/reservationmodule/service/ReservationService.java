package com.uolproject.reservationmodule.service;

import org.springframework.data.repository.CrudRepository;
import com.uolproject.reservationmodule.model.Reservation;

public interface ReservationService extends CrudRepository<Reservation, Integer> {

}
