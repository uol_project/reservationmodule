package com.uolproject.reservationmodule.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uolproject.reservationmodule.model.Reservation;
import com.uolproject.reservationmodule.model.ScheduleTemplate; 
             
@Component
public class ScheduleBuilder  {
	
	@Autowired
    private ReservationService reservationService;
	
	public void generateSchedule(ScheduleTemplate scheduleTemplate){
		System.out.println(scheduleTemplate);
		
		for (int i = 0; i < scheduleTemplate.getAppointmentCount(); i++) {
		
			Reservation reservation = new Reservation();
			reservation.setDescription("scheduleDate=" + scheduleTemplate.getScheduleDate() 
				+ ", startTime=" 
				+ scheduleTemplate.getStartTime().plusMinutes(i*scheduleTemplate.getDurationInMins()));
			reservation.setLastModified(LocalDateTime.now());
			reservation.setCreatedAt(LocalDateTime.now());
			reservation.setReservationStartAt(
					scheduleTemplate.getStartTime().plusMinutes(i*scheduleTemplate.getDurationInMins())
			);
			reservation.setDurationInMins(scheduleTemplate.getDurationInMins());
			
			reservationService.save(reservation);
		}
	}
	

}
