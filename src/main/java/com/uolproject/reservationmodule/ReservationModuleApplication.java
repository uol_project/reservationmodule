package com.uolproject.reservationmodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReservationModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservationModuleApplication.class, args);
	}

}
