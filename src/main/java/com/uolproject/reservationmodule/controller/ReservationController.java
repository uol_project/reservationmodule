package com.uolproject.reservationmodule.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.uolproject.reservationmodule.model.Reservation;
import com.uolproject.reservationmodule.service.ReservationService;


@RestController
public class ReservationController {
	
	@Autowired
	ReservationService reservationService;
	
	@PostMapping("/reservation")
	Reservation create(@RequestBody Reservation reservation) {
		return reservationService.save(reservation);
	}

	@GetMapping("/reservation")
	Iterable<Reservation> read() {
		return reservationService.findAll();
	}
    
	@GetMapping("/reservation/{id}")
	Optional<Reservation> find(@PathVariable Integer id) {
        return reservationService.findById(id);
    }
    
	@PutMapping("/reservation")
	Reservation update(@RequestBody Reservation reservation) {
		return reservationService.save(reservation);
	}
	
	@DeleteMapping("/reservation/{id}")
	void delete(@PathVariable Integer id) {
		reservationService.deleteById(id);
	}
	
}
