package com.uolproject.reservationmodule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.uolproject.reservationmodule.model.ScheduleTemplate;
import com.uolproject.reservationmodule.service.ScheduleBuilder;

@RestController
public class ScheduleController {

    @Autowired
    private ScheduleBuilder scheduleBuilder;

	@PostMapping("/schedule")
	private void  generateSchedule(@RequestBody ScheduleTemplate scheduleTemplate) {
		scheduleBuilder.generateSchedule(scheduleTemplate);
	}

}
